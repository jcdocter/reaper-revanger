using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    public float groundDistance = 0.4f;

    public float speed = 6f;

    public float smoothTime = 0.1f;

    public LayerMask groundMask;

    public Transform camera;
    public Transform groundCheck;

    public CharacterController characterController;

    private PlayerControls controls;
    private PlayerAnimation playerAnimation;

    private Vector3 movePlayer;
    private Vector3 rotatePlayer;
    private Vector3 velocity;

    private float turnSmoothVelocity;
    private bool isGrounded;

    void Awake()
    {
        playerAnimation = GetComponent<PlayerAnimation>();

        controls = new PlayerControls();

         controls.Gameplay.Move.performed += ctx => movePlayer = ctx.ReadValue<Vector2>();
         controls.Gameplay.Rotate.performed += ctx => rotatePlayer = ctx.ReadValue<Vector2>();
         controls.Gameplay.Jump.performed += ctx => Jump();

         controls.Gameplay.Move.canceled += ctx => movePlayer = Vector2.zero;
         controls.Gameplay.Rotate.canceled += ctx => rotatePlayer = Vector2.zero;
    }

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        Vector3 move = new Vector3(movePlayer.x, 0f,movePlayer.y).normalized;

        Vector3 rotate = new Vector3(rotatePlayer.y, rotatePlayer.x) * 100f * Time.deltaTime;

        velocity.y += gravity * Time.deltaTime;

        characterController.Move(velocity * Time.deltaTime);

        if(move.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(move.x, move.z) * Mathf.Rad2Deg + camera.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, smoothTime);

            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            characterController.Move(moveDirection.normalized * speed * Time.deltaTime);
            playerAnimation.Walk(true);
        }
        else
        {
            playerAnimation.Walk(false);
        }
    }

     void Jump()
     {
        if(isGrounded && velocity.y < 0f)
        {
            velocity.y = -2f;
            playerAnimation.Jump();
        }

        if (isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
     }

    void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    void OnDisable()
    {
        controls.Gameplay.Disable();
    }
}
