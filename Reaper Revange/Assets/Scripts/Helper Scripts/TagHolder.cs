public class Axis
{
    public const string HORIZONTAL = "Horizontal";
    public const string VERTICAL = "Vertical";

  //  public const string MOUSEX = "Mouse X";
  //  public const string MOUSEY = "Mouse Y";
}

public class Tags
{
    public const string PLAYER_TAG = "Player";
    public const string COLLECTABLE_TAG = "Collectable";
}

public class AnimationTags
{
    public const string WALK_PARAMETER = "Walk";
    public const string JUMP_PARAMETER = "Jump";
}
