using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    private Animator anim;
    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void Walk(bool move)
    {
        anim.SetBool(AnimationTags.WALK_PARAMETER, move);
    }

    public void Jump()
    {
        anim.SetTrigger(AnimationTags.JUMP_PARAMETER);
    }
}
